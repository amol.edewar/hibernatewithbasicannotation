/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.testproject;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author krawler
 */

@Entity 
@Table(name="student")
public class Invoice_Test {
    
    @Id
    private int invid;
    private String numer;
    private String description;
    @OneToMany(mappedBy = "std")
    private List<Laptop> laptop=new ArrayList<>();
   

    

    /**
     * @return the numer
     */
    public String getNumer() {
        return numer;
    }

    /**
     * @param numer the numer to set
     */
    public void setNumer(String numer) {
        this.numer = numer;
    }

   

    /**
     * @return the invid
     */
    public int getInvid() {
        return invid;
    }

    /**
     * @param invid the invid to set
     */
    public void setInvid(int invid) {
        this.invid = invid;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the laptop
     */
    public List<Laptop> getLaptop() {
        return laptop;
    }

    /**
     * @param laptop the laptop to set
     */
    public void setLaptop(List<Laptop> laptop) {
        this.laptop = laptop;
    }

   

   

    
}
