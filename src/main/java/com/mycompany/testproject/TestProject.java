/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.testproject;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import org.hibernate.cfg.Configuration;

/**
 *
 * @author krawler
 */
public class TestProject {
    public static void main(String [] args){
     
     
     
     Laptop laptop=new Laptop();
     laptop.setLid(1);
     laptop.setLname("dell");
     
     Invoice_Test inv =new Invoice_Test();
     inv.setInvid(101);
     inv.setNumer("Inv01");
     inv.setDescription("test inv");
     inv.getLaptop().add(laptop);
    
     
        Configuration cfg=new Configuration().configure().addAnnotatedClass(Invoice_Test.class).addAnnotatedClass(Laptop.class);
        SessionFactory sf=cfg.buildSessionFactory();
        Session session=sf.openSession();
       session.beginTransaction();
        session.save(inv);
        session.save(laptop);
        session.getTransaction().commit();
        session.close();
        
    }
    
}
